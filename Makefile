# Source code directory.
SRC=src
# Distribution code directory.
DIST=dist
# Build tools directory.
SLURP=Slurp

# Colors
RED=\033[0;31m
GREEN=\033[0;32m
NC=\033[0m

default:
	@# Runs the web app for development.
	@mkdir -p $(DIST)
	@make -s build_dev
	cd $(DIST) && python3 -m http.server 5000

build_dev:
	@# Fast build for development.
	python3 $(SLURP)/fileMonitor.py $(SRC) --destinationPath=$(DIST) \
	| $(SLURP)/fileLinker.py --fileExtensions=js,html,css --stopOnLoop=True \
	| $(SLURP)/ignore.py node_modules \
	| $(SLURP)/spit.py &
	
install:
	@# Installs dependencies.
	apt-get update
	npm install
	pip3 install -r requirements.txt
	git submodule update --init --recursive

docs:
	@# Creates JS documentation.
	@documentation build --format=html --output=documentation $(SRC)

update_submodules:
	@# Recursively brings submodules up to the latest version.
	git submodule update --recursive --remote --merge

test:
	xdg-open http://localhost:5000/test/
