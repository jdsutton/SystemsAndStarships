[[Asteroid.js]]
[[Faction.js]]
[[parts/BasicHelm.js]]
[[parts/SmallCargoHold.js]]
[[parts/SmallShipPort.js]]
[[parts/SmallShuttle.js]]
[[parts/TinyThruster.js]]
[[Spaceship.js]]
[[Location.js]]
[[ui/ShipConstructionInterface.js]]

const playerShip = new Spaceship();
playerShip.setFaction(new Faction());
playerShip.setRoot(new SmallShuttle());

const faction = new Faction();
faction.setName('Rinky-dink Shipyards and Mortuary');
faction.addBlueprint(SmallCargoHold);
faction.addBlueprint(BasicHelm);
faction.addBlueprint(TinyThruster);
faction.addBlueprint(SmallShuttle);

const shipyard = new Spaceship();
shipyard.setRoot(new SmallShipPort());
shipyard.setFaction(faction);
shipyard.setName('RSM Scrapheap');

const loc = new Location(0, 0);
loc.addNearbyShip(shipyard);
loc.autoPopulate();
for (let i = 0; i < 32; i++) {
    const asteroid = new Asteroid(Math.random() * 1000 - 500, Math.random() * 1000 - 500);
    loc.addNearbyObject(asteroid);
}

GameController.currentLocation = loc;
// GameController.userInterface = new ShipConstructionInterface(shipyard);
GameController.playerShip = playerShip;