[[ShipOverviewInterface.js]]
[[UserInterface.js]]

class RadarUserInterface extends UserInterface {
    render() {
        const pShip = GameController.playerShip;
        // Radar.
        const radarRadius = 1000;
        noFill();
        stroke(255);
        ellipse(width - 48, height - 48, 48, 48);
        point(width - 48, height - 48);

        GameController.currentLocation._nearbyShips.forEach(ship => {
            if (dist(pShip.x, pShip.y, ship.x, ship.y) < radarRadius) {
                point(width - 48 + (ship.x - pShip.x) * 24 / radarRadius,
                    height - 48 + (ship.y - pShip.y) * 24 / radarRadius);
            }
        });

        GameController.currentLocation._nearbyObjects.forEach(obj => {
            if (dist(pShip.x, pShip.y, obj.x, obj.y) < radarRadius) {
                point(width - 48 + (obj.x - pShip.x) * 24 / radarRadius,
                    height - 48 + (obj.y - pShip.y) * 24 / radarRadius);
            }
        });

        // Fuel indicator.
        const fuelLevel = pShip.fuel / pShip.fuelCapacity;
        fill(255, 153, 0);
        noStroke();
        rect(0, 0, 64, 32, 16);
        rect(80, 0, Math.max(0, fuelLevel) * width / 2, 32, 16);
        fill(0);
        rect(32, 0, 70, 32);
        fill(51, 102, 204);
        textSize(32);
        text('Fuel', 36, 26);
    }

    onClick() {
        GameController.currentLocation._nearbyShips.forEach(ship => {
            if (ship.mouseIsInside()) {
                GameController.userInterface = new ShipOverviewInterface(ship);
                return;
            }
        });
    }
}