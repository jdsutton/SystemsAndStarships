[[Button.js]]
[[UserInterface.js]]

class ShipOverviewInterface extends UserInterface {
    constructor(ship) {
        let buttons = [];
        const hailButton = new Button({
            x: GameController.width - 100,
            y: GameController.height - 150,
            w: 80,
            h: 40,
            label: 'Hail',
            onClick: () => {
                console.log('Hail!');
            }
        });
        buttons.push(hailButton);

        if (ship.canConstruct()) {
            const constructButton = new Button({
                x: GameController.width - 100,
                y: GameController.height - 100,
                w: 80,
                h: 40,
                label: 'Construct',
                onClick: () => {
                    GameController.userInterface = new ShipConstructionInterface(ship);
                }
            });

            buttons.push(constructButton)
        }

        const exitButton = new Button({
            x: GameController.width - 100,
            y: GameController.height - 50,
            w: 80,
            h: 40,
            label: 'Exit',
            onClick: () => {
                GameController.userInterface = new RadarUserInterface();
            }
        });
        buttons.push(exitButton);


        super(buttons);

        this.ship = ship;
    }

    onClick() {
        GameController.currentLocation._nearbyShips.forEach(ship => {
            if (ship.mouseIsInside()) {
                GameController.userInterface = new ShipOverviewInterface(ship);
                return;
            }
        });
    }

    render() {
        super.render();

        textAlign(LEFT, BASELINE);
        noStroke();
        fill(255);
        textSize(24);
        text(this.ship.name, 16, 24);
        textSize(18);
        text(this.ship.faction.name, 16, 48);
    }
}