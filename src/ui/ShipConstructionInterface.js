[[UserInterface.js]]

class ShipConstructionInterface extends UserInterface {
    constructor(shipyard) {
        super();

        this._shipyard = shipyard;
        this._blueprints = shipyard.faction.partBlueprints.map(bp => new bp());
    }

    onclick() {

    }

    render() {
        // Render parts list.
        push();
        translate(48, GameController.height - 48);
        this._blueprints.forEach(blueprint => {
            blueprint.render(32, 32);
            translate(128, 0);
        });
        pop();

        // Render credits available.
        fill(255);
        textAlign(LEFT, BASELINE);
        text('Credits: ' + faction.funds, 16, 16);

        // Render cost so far.
        fill(255);
        text('Cost as built: ' + 0, 16, 32);

        // Render construct button.
    }

    show() {
        
    }
}