class UserInterface {
    constructor(elements) {
        this.elements = elements;
    }

    onClick() {
        this.elements.forEach(element => {
            if (element.mouseIsInside()) {
                element.onClick();
            }
        })
    }

    render() {
        this.elements.forEach(element => {
            element.render();
        });
    }
}