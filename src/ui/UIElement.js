class UIElement {
    constructor(x, y, w, h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    mouseIsInside() {
        return (mouseX >= this.x && mouseX < this.x + this.w
            && mouseY >= this.y && mouseY < this.y + this.h);
    }

    onClick() {
        
    }
}