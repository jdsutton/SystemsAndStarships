[[UIElement.js]]

class Button extends UIElement {
    constructor(config) {
        super(config.x, config.y, config.w, config.h);
        this.label = config.label;
        this.onClick = config.onClick;
    }

    render() {
        fill(255);
        noStroke();
        rect(this.x, this.y, this.w, this.h);
        fill(0);
        noStroke();
        textSize(12);
        textAlign(CENTER, CENTER);
        text(this.label, this.x + this.w / 2, this.y + this.h / 2)
    }
}