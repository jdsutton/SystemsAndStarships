[[ui/RadarUserInterface.js]]

class GameController {
    static setDimensions(w, h) {
        GameController.width = w;
        GameController.height = h;
    }

    static step() {
        background(0);

        push();
        translate(GameController.width / 2 - playerShip.x, GameController.height / 2 - playerShip.y);

        GameController.currentLocation.render();

        playerShip.control();
        if (GameController.playerShip) {
            playerShip.render();
        }

        pop();

        if (GameController.userInterface) {
            GameController.userInterface.render();
        }
    }

    static onClick() {
        GameController.userInterface.onClick();
    }

    static getOnScreenCoords(x, y) {
        return [
            x - GameController.width / 2 + playerShip.x,
            y - GameController.height / 2 + playerShip.y
        ];
    }
}

GameController.width = null;
GameController.height = null;
GameController.currentLocation = null;
GameController.userInterface = new RadarUserInterface();
GameController.playerShip = null;
GameController.keys = {};

[[startingLocation.js]]