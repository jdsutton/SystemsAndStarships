class Asteroid {
    constructor(x, y) {;
        this.x = x;
        this.y = y;
        this.theta = 0;
        this.vTheta = Math.random() * 0.01 - 0.005;
        this.vx = Math.random() * 2 - 1;
        this.vy = Math.random() * 2 - 1;
        this._baseRadius = Math.random() * 128 + 16;
        this.mass = Math.pow(this._baseRadius, 2);
        this._points = [];

        let theta = 0;
        while (theta < Math.PI * 2) {
            const radius = this._baseRadius * (0.9 + Math.random() * 0.2);

            this._points.push([
                radius * Math.cos(theta),
                radius * Math.sin(theta)
            ]);

            theta += Math.random() * Math.PI / 16 + Math.PI / 16;
        }
    }

    render() {
        push();
        translate(this.x, this.y);
        rotate(this.theta);

        fill(88, 73, 65);
        stroke(103, 68, 50);
        strokeWeight(2);

        beginShape();
        this._points.forEach(point => {
            vertex(point[0], point[1]);
        });
        endShape(CLOSE);

        pop();

        this.x += this.vx;
        this.y += this.vy;
        this.theta += this.vTheta;
    }
}