[[Spaceship.js]]

class Location {
    constructor(x, y) {
        this._x = x;
        this._y = y;
        this._z = 0;
        this._nearbyShips = [];
        this._nearbyObjects = [];
    }

    addNearbyShip(ship) {
        this._nearbyShips.push(ship);
    }

    addNearbyObject(obj) {
        this._nearbyObjects.push(obj);
    }

    render() {
        this._nearbyObjects.forEach(obj => {
            obj.render();
        });

        this._nearbyShips.forEach(ship => {
            ship.render();
        });

        this._nearbyShips.forEach(ship => {
            if (ship.mouseIsInside()) {
                fill(255);
                noStroke();
                const [x, y] = GameController.getOnScreenCoords(mouseX, mouseY);
                textSize(16);
                text(ship.name, x, y);
                if (ship.faction) {
                    text(ship.faction.name, x, y + 16);
                }
            }
        });

    }

    autoPopulate() {
        for (let i = 0; i < 16; i++) {
            let ship = Spaceship.generate();
            ship.x = Math.random() * 1000 - 500;
            ship.y = Math.random() * 1000 - 500;
            ship.theta = Math.random() * Math.PI * 2;
            ship.vx = Math.random() * 2 - 1;
            ship.vy = Math.random() * 2 - 1;
            this._nearbyShips.push(ship);
        }
    }
}