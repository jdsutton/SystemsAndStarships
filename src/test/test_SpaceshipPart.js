[[__top__/src/test/assert.js]]
[[__top__/src/Spaceship.js]]
[[__top__/src/SpaceshipPart.js]]

let dist = (x, y, xP, yP) => {
    x -= xP;
    y -= yP;

    return Math.sqrt(x*x + y*y);
}

function test_getGlobalCoords() {
    let ship = new Spaceship();
    let part = new SpaceshipPart();
    ship.setRoot(part);

    let [x, y, theta] = part.getGlobalCoords();
    assertEqual(x, 0);
    assertEqual(y, 0);
    assertEqual(theta, 0);

    let mount = new SpaceshipPart.MountPoint(16, 0);
    let childMount = new SpaceshipPart.MountPoint(0, 0);
    let childPart = new SpaceshipPart();
    part.addChild(childPart, mount, childMount);

    [x, y, theta] = childPart.getGlobalCoords();
    assertEqual(x, 16);
    assertEqual(y, 0);
    assertEqual(theta, 0);

    ship.theta = Math.PI / 2;

    [x, y, theta] = part.getGlobalCoords();
    assertEqual(x, 0);
    assertEqual(y, 0);
    assertEqual(theta, Math.PI / 2);

    [x, y, theta] = childPart.getGlobalCoords();
    assertEqual(Math.round(x), 0);
    assertEqual(Math.round(y), 16);
    assertEqual(theta, Math.PI / 2);
} 

function test_getNearybyMountPoints() {
    let ship = new Spaceship();
    let part = new SpaceshipPart();
    ship.setRoot(part);
    let shipMount = new SpaceshipPart.MountPoint(16, 0, part)
    shipMount.getGlobalCoords = () => {
        return [16, 0, null];
    }

    part.getOpenMountPoints = () => {
        return [shipMount];
    };

    let refrencePart = new SpaceshipPart();
    let refrenceMount = new SpaceshipPart.MountPoint(10, Math.PI, refrencePart);
    refrenceMount.getGlobalCoords = () => {
        return [20, 0, null];
    }

    let result = refrencePart.getNearbyMountPoints(refrenceMount, ship);
    assertEqual(result.length, 1);
    assertEqual(result[0], shipMount);

    refrenceMount.getGlobalCoords = () => {
        return [48, 0, null];
    }

    result = refrencePart.getNearbyMountPoints(refrenceMount, ship);
    assertEqual(result.length, 0);
}