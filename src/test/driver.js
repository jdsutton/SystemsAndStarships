window.onload = function() {
    for (let key in window) {
        if (typeof(window[key]) === 'function' && key.startsWith('test_')) {
            let result = document.createElement('div');
            try {
                window[key]();
                result.innerHTML = 'Test passed: ' + key;
                result.style.color = '#00AA00';
            }
            catch (e) {
                result.innerHTML = e.stack + '\n' + e;
                result.style.color = '#AA0000';
            }
            document.body.appendChild(result);
        }
    }
}
