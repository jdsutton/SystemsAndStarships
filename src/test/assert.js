function assertEqual(a, b) {
    if (a !== b) {
        throw new Error(a + ' is not equal to ' + b);
    }
}