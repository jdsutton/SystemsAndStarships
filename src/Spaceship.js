[[Faction.js]]
[[Keys.js]]
[[parts/BasicHelm.js]]
[[parts/InertialDampener.js]]
[[parts/SmallCargoHold.js]]
[[parts/SmallShuttle.js]]
[[parts/TinyThruster.js]]
[[util.js]]

class Spaceship {
    constructor(x, y) {
        this._root = null;
        this.name = Spaceship.getName();
        this._autopilot = false;
        this._autotrade = false;
        this._attackOnSight = [];
        this._combatBehavior = Spaceship.BEHAVIOR.FLEE;
        this._inventory = [];
        this._inventoryCapacity = 0;
        this._mass = 0;
        this.fuelCapacity = 0;
        this.fuelEfficiency = 1.0;
        this.fuel = 0;
        this._thrustForce = 0;
        this.faction = new Faction();
        this.x = x || 0;
        this.y = y || 0;
        this.vx = 0;
        this.vy = 0;
        this.vTheta = 0;
        this.theta = 0;
    }

    canConstruct() {
        // TODO.
        return true;
    }

    control() {
        if (this.fuel > 0) {
            if (GameController.keys[KEYS.W]) {
                // Thrust forward.
                this.vy += Math.sin(this.theta) * this._thrustForce / this._mass;
                this.vx += Math.cos(this.theta) * this._thrustForce / this._mass;
                this.fuel -= 1.0 / this.fuelEfficiency;
            }
            if (GameController.keys[KEYS.A]) {
                // Rotate left.
                this.vTheta -= 0.002 * this._thrustForce / this._mass;
                this.fuel -= 1.0 / this.fuelEfficiency;
            }
            if (GameController.keys[KEYS.S]) {
                // Thrust backward.
                this.vy -= Math.sin(this.theta) * this._thrustForce / this._mass;
                this.vx -= Math.cos(this.theta) * this._thrustForce / this._mass;
                this.fuel -= 1.0 / this.fuelEfficiency;
            }
            if (GameController.keys[KEYS.D]) {
                // Rotate right.
                this.vTheta += 0.002 * this._thrustForce / this._mass;
                this.fuel -= 1.0 / this.fuelEfficiency;
            }
            if (GameController.keys[KEYS.Q]) {
                // Strafe left.
                this.vy += Math.sin(this.theta - Math.PI / 2) * this._thrustForce / this._mass;
                this.vx += Math.cos(this.theta - Math.PI / 2) * this._thrustForce / this._mass;
                this.fuel -= 1.0 / this.fuelEfficiency;
            }
            if (GameController.keys[KEYS.E]) {
                // Strafe right.
                this.vy += Math.sin(this.theta + Math.PI / 2) * this._thrustForce / this._mass;
                this.vx += Math.cos(this.theta + Math.PI / 2) * this._thrustForce / this._mass;
                this.fuel -= 1.0 / this.fuelEfficiency;
            }
        }
    }

    getParts() {
        let parts = [this._root];
        let allParts = [];

        while (parts.length > 0) {
            let part = parts.pop();
            allParts = allParts.concat(part);
            parts = parts.concat(part.getChildren())
        }

        return allParts;
    }

    getOpenMountPoints() {
        let points = [];

        this.getParts().forEach(part => {
            points = points.concat(part.getOpenMountPoints());
        })

        return points;
    }

    mouseIsInside() {
        return this._root.mouseIsInside();
    }

    setName(name) {
        this.name = name;
    }

    setRoot(part) {
        this._root = part;
        part.setShip(this);

        this._thrustForce = part.getThrustForce();
        this.fuelCapacity = part.getFuelCapacity();
        this.fuel = this.fuelCapacity;
        this.fuelEfficiency = part.getFuelEfficiency();
        this._mass = part.getMass();
    }

    setFaction(faction) {
        this.faction = faction;
    }

    step() {

    }

    render() {
        push();
        translate(this.x, this.y);
        rotate(this.theta);
        this._root.render();

        if (this.mouseIsInside()) {
            stroke(0, 200, 0);
            strokeWeight(2);
            noFill();
            rotate(millis() / 1000);
            ellipse(0, 0, 100, 100);
            line(-50, 0, -34, 0);
            line(50, 0, 34, 0);
        }
        pop();

        this.x += this.vx;
        this.y += this.vy;
        this.theta += this.vTheta;
    }

    static generate() {
        const parts = [
            BasicHelm,
            SmallCargoHold,
            TinyThruster,
            InertialDampener
        ];
        let result = new Spaceship();
        let part = new BasicHelm();
        result.setRoot(part);
        let queue = [[part, 3]];

        while (queue.length > 0) {
            let [currentPart, depthRemaining] = queue.pop();
            if (depthRemaining < 1) {
                continue;
            }
            const mountPoints = currentPart.getMountPoints();

            mountPoints.forEach(point => {
                if (!currentPart.getMountPointInUse(point)) {
                    let newPart = new (choice(parts))();
                    currentPart.addChild(newPart, point, choice(newPart.getMountPoints()));

                    queue.push([newPart, depthRemaining - 1]);
                }
            });
        }

        return result;
    }

    static getName() {
        return 'The ' + choice(Spaceship.NAME_PREFIX) + ' ' + choice(Spaceship.NAME_SUFFIX);
    }
}

Spaceship.NAME_PREFIX = [
    'Dark',
    'Desolate',
    'Flaming',
    'Rising',
    'Sunken',
    'Shattered',
    'Glorious',
    'Intrepid',
    'Shooting',
    'Holy',
    'Legendary',
    'Xzlrr',
    'Rickety',
    'Patchwork',
    'Bright',
    'Shining',
    'Endless',
    'Never',
    'Dread',
    'Piercing',
    'Splattered',
    'Dusty',
];

Spaceship.NAME_SUFFIX = [
    'Star',
    'Nebula',
    'Sun',
    'Galaxy',
    'Comet',
    'Thief',
    'Raider',
    'Ray',
    'Nightmare',
    'Freighter',
    'Racer',
    'Cube',
    'Lancer',
    'Knight',
    'Xlorb',
    'Death',
    'Starliner',
    'Clunker',
    'Starburst',
    'Spud',
    'Worm',
    'Voyager',
    'Wormhole',
];

Spaceship.BEHAVIOR = {
    FLEE: 0,
    SEEK_AND_DESTORY: 1,
    EXPLORE: 2,
    TRADE: 3
};