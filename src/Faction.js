[[util.js]]

class Faction {
    constructor() {
        this.name = Faction.generateFactionName();
        this._shipPrototypes = [];
        this.partBlueprints = [];
        // for (let i = 0; i < 6; i++) {
        //     this._shipPrototypes.push(Spaceship.generate());
        // }
        this.funds = Faction.DEFAULT_FUNDS;
        this._primaryValue = choice(Object.keys(Faction.VALUES));
    }

    addBlueprint(blueprint) {
        this.partBlueprints.push(blueprint);
    }

    setName(name) {
        this.name = name;
    }

    getWealthDescription() {
        // TODO
        return 'poor';
    }

    getHappinessDescription() {
        // TODO
        return Faction.HAPPINESS.VERY_UNHAPPY;
    }

    getDescription() {
        let result = '';

        result = result + 'The government of ' + this.name + ' is ' + this.getWealthDescription() + '\n'; 
        result = result + 'The people of ' + this.name + ' value ' + this._primaryValue + '\n';
        result = result + 'The people of ' + this.name + ' are ' + this.getHappinessDescription();

        return result;
    }

    static generateFactionName() {
        return choice(Faction.NAME_PREFIX_PART) + '-' + choice(Faction.NAME_PREFIX_PART) + ' ' + choice(Faction.NAME_SUFFIX);
    }
}

Faction.DEFAULT_FUNDS = 1000000;

Faction.NAME_PREFIX_PART = [
    'Alpha',
    'Glo',
    'K',
    'Kling',
    'On',
    'Pax',
    'Plax',
    'Uni',
    'Xeno',
    'Zal'
];

Faction.NAME_SUFFIX = [
    'Alliance',
    'Armada',
    'Army',
    'Conglomerate',
    'Empire',
    'Federation',
    'Hivemind',
    'Incorporated',
    'Kingdom',
    'Militia'
];

Faction.VALUES = {
    PEACE: 'Peace',
    SECURITY: 'Security',
    CONQUEST: 'Conquest',
    MONEY: 'Money',
    ART: 'Art',
    KNOWLEDGE: 'Knowledge'
}

Faction.HAPPINESS = {
    VERY_UNHAPPY: 'very unhappy',
    UNHAPPY: 'unhappy',
    UNEASY: 'uneasy',
    STOIC: 'stoic',
    PLEASE: 'pleased',
    HAPPY: 'happy',
    VERY_HAPPY: 'very happy'
};

Faction.PRIVACY_LEVEL = {
    HOSTILE: 'hostile',
    GAURDED: 'gaurded',
    BALANCED: 'balanced',
    OPEN: 'open',
    TMI: 'TMI'
};

Faction.AGRESSION_LEVEL = {
    PACIFIST: 'pacifist',
    TIMID: 'timid',
    BALANCED: 'balanced',
    IRRITABLE: 'irritable',
    HOSTILE: 'hostile',
};