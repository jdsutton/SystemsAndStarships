[[__top__/src/SpaceshipPart.js]]

class SmallShuttle extends SpaceshipPart {
    constructor() {
        super(64, 32);

        this._mass = 1;
        this._thrustForce = 0.1;
        this._health = 10;
        this._maxHealth = 10;
        this._energyConsumption = 1;
        this._operationCost = 0;
        this._inventoryCapacity = 5;
        this._fuelCapacity = 150;
        this._energyCapacity = 100;
        this._crewCapacity = 1;
    }

    render(maxWidth=1000, maxHeight=1000) {
        const w = Math.min(this.width, maxWidth);
        const h = Math.min(this.height, maxHeight);

        // Base.
        fill(200);
        stroke(50);
        rect(-w / 2, -h / 2, w, h);

        // Engine pylons.
        rect(-w / 2, -h, w / 2, h / 2);
        rect(-w / 2, h / 2, w / 2, h / 2);

        // Bridge.

        super.render();
    }
}

SmallShuttle.mountPoints = [];