[[__top__/src/SpaceshipPart.js]]

class SmallCargoHold extends SpaceshipPart {
    constructor() {
        super(64, 64);
    }

    render(maxWidth=1000, maxHeight=1000) {
        const w = Math.min(this.width, maxWidth);
        const h = Math.min(w, maxHeight);

        // Base.
        fill(200);
        stroke(50);
        rect(-w / 2, -h / 2, w, h);
        rect(-w / 6, -h / 6, w / 3, h / 3);

        super.render();
    }
}

SmallCargoHold.mountPoints = [
    new SpaceshipPart.MountPoint(64 / 2, 0),
    new SpaceshipPart.MountPoint(64 / 2, Math.PI),
    new SpaceshipPart.MountPoint(64 / 2, Math.PI / 2),
    new SpaceshipPart.MountPoint(64 / 2, Math.PI * 1.5)
];