[[__top__/src/SpaceshipPart.js]]

class SmallShipPort extends SpaceshipPart {
    constructor() {
        super(128, 128);
    }

    render(maxWidth=1000, maxHeight=1000) {
        const w = Math.min(this.width, maxWidth);
        const h = Math.min(w, maxHeight);

        const sideW = Math.round(w * 0.1);

        push();
        translate(-w / 2, -h / 2);

        // Base.
        fill(200);
        stroke(50);
        rect(0, 0, w, h);
        rect(w / 3, h / 3, w / 3, h / 3);
        ellipse(w / 2, h / 2, w / 3, h / 3);

        const alpha = (Math.sin(millis() / 1000) + 1) / 2 * 255;
        stroke(255, 204, 0, alpha);
        strokeWeight(2);
        line(w / 2 - w / 6 + 1, h / 2, w / 2 + w / 6 - 1, h / 2);

        // Sides.
        stroke(50);
        strokeWeight(1);
        fill(150);
        
        quad(
            sideW, 0,
            sideW, h / 3,
            sideW * 2, h / 6 + h / 12,
            sideW * 2, h / 6 - h / 12);

        quad(
            sideW, h - h / 3,
            sideW, h,
            sideW * 2, h * (5 / 6) + h / 12,
            sideW * 2, h * (5 / 6) - h / 12);

        quad(
            w - sideW, h / 2 - h / 6,
            w - sideW, h / 2 + h / 6,
            w - sideW * 2, h / 2 + h / 12,
            w - sideW * 2, h / 2 - h / 12);

        rect(0, 0, sideW, h);
        rect(w - sideW, 0, sideW, h);

        pop();

        super.render();
    }
}