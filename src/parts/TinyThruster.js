[[__top__/src/SpaceshipPart.js]]

class TinyThruster extends SpaceshipPart {
    constructor() {
        super(32, 16);
    }

    render(maxWidth=1000, maxHeight=1000) {
        const w = Math.min(this.width, maxWidth);
        const h = Math.min(this.height, maxHeight);

        // Base.
        fill(200);
        stroke(50);
        rect(-w / 2, -h / 2, w, h);

        super.render();
    }
}

TinyThruster.mountPoints = [
    new SpaceshipPart.MountPoint(32 / 2, 0),
];