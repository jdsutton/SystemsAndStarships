[[__top__/src/SpaceshipPart.js]]

class InertialDampener extends SpaceshipPart {
    constructor() {
        super(32, 64);
    }

    render(maxWidth=1000, maxHeight=1000) {
        const w = Math.min(this.width, maxWidth);
        const h = Math.min(this.height, maxHeight);

        // Base.
        fill(200);
        stroke(50);
        rect(-w / 2, -h / 2, w, h);

        super.render();
    }
}

InertialDampener.mountPoints = [
    new SpaceshipPart.MountPoint(32 / 2, 0),
    new SpaceshipPart.MountPoint(32 / 2, Math.PI),
    new SpaceshipPart.MountPoint(64 / 2, Math.PI / 2),
    new SpaceshipPart.MountPoint(64 / 2, Math.PI * 1.5)
];