[[__top__/src/SpaceshipPart.js]]

class BasicHelm extends SpaceshipPart {
    constructor() {
        super(32, 32);
    }
    
    render(maxWidth=1000, maxHeight=1000) {
        const w = Math.min(this.width, maxWidth);
        const h = Math.min(w, maxHeight);

        // Base.
        fill(200);
        stroke(50);
        quad(
            -w / 2, -h / 2,
            -w / 2, h / 2,
            w / 2, h / 3,
            w / 2, -h / 3
        );

        super.render();
    }
}

BasicHelm.mountPoints = [
    new SpaceshipPart.MountPoint(32 / 2, Math.PI),
];