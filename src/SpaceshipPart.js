class SpaceshipPart {
    constructor(width, height) {
        this._children = [];
        this._mass = 0;
        this._thrustForce = 0;
        this._health = 0;
        this._maxHealth = 0;
        this._energyConsumption = 0;
        this._operationCost = 0;
        this._inventoryCapacity = 0;
        this._fuelCapacity = 0;
        this._fuelEfficiency = 100.0;
        this._energyCapacity = 0;
        this._crewCapacity = 0;
        this.width = width;
        this.height = height;
        this.ship = null;
        this.parent = null;
        this.mountPointsInUse = new Set();
        // Offset relative to the parent part.
        this.offsetX = 0;
        this.offsetY = 0;
        this.offsetTheta = 0;
        this.offsetRadius = 0;
    }

    getChildren() {
        return this._children.slice();
    }

    setShip(ship) {
        /**
         * Sets the ship this part belongs to.
         * @param {Spaceship} ship
         */
        this.ship = ship;

        this._children.forEach(child => {
            child.setShip(ship);
        });
    }

    getMass() {
        let result = this._mass;

        this._children.forEach(child => {
            result += child.getMass();
        });

        return result;
    }

    getThrustForce() {
        let result = this._thrustForce;

        this._children.forEach(child => {
            result += child.getThrustForce();
        });

        return result;
    }

    getFuelCapacity() {
        let result = this._fuelCapacity;

        this._children.forEach(child => {
            result += child.getFuelCapacity();
        });

        return result;
    }

    getFuelEfficiency() {
        let result = this._fuelEfficiency * this._thrustForce;

        this._children.forEach(child => {
            result += child.getFuelEfficiency();
        });

        return result / this.getThrustForce();
    }

    getGlobalCoords() {
        let x, y, theta;
        if (this.parent) {
            [x, y, theta] = this.parent.getGlobalCoords();
        } else {
            x = this.ship.x;
            y = this.ship.y;
            theta = this.ship.theta;
        }

        x += this.offsetRadius * Math.cos(theta);
        y += this.offsetRadius * Math.sin(theta);
        theta += this.offsetTheta;

        return [x, y, theta];
    }

    getNearbyMountPoints(mountPoint, ship) {
        const parts = ship.getParts();
        const [x, y, _] = mountPoint.getGlobalCoords();
        let result = [];

        parts.forEach(part => {
            const mounts = part.getOpenMountPoints();
            mounts.forEach(mount => {
                const [otherX, otherY, _] = mount.getGlobalCoords();
                if (dist(x, y, otherX, otherY) < 32) {
                    result.push(mount);
                }
            });
        });

        return result;
    }

    addChild(childPart, mountPoint, childPartMountPoint) {
        const offsetRadius = mountPoint.radius + childPartMountPoint.radius;
        childPart.offsetX = offsetRadius * Math.cos(mountPoint.theta);
        childPart.offsetY = offsetRadius * Math.sin(mountPoint.theta);
        childPart.offsetTheta = mountPoint.theta - childPartMountPoint.theta;
        childPart.offsetRadius = offsetRadius;
        childPart.ship = this.ship;
        childPart.parent = this;

        this.setMountPointInUse(mountPoint);
        childPart.setMountPointInUse(childPartMountPoint);

        this._children.push(childPart);
    }

    render(maxWidth=1000, maxHeight=1000) {
        this._children.forEach(child => {
            push();
            translate(child.offsetX, child.offsetY);
            rotate(child.offsetTheta + Math.PI);
            child.render();
            pop();
        });
    }

    getMountPoints() {
        return this.constructor.mountPoints.map(point => {
            return point.setPart(this);
        })
    }

    getOpenMountPoints() {
        return this.getMountPoints().filter(point => {
            return !this.getMountPointInUse(point);
        })
    }

    setMountPointInUse(mountPoint) {
        this.mountPointsInUse.add(mountPoint);
    }

    getMountPointInUse(mountPoint) {
        return this.mountPointsInUse.has(mountPoint);
    }

    getOnscreenCoords() {
        let [x, y, _] = this.getGlobalCoords();

        return [
            GameController.width / 2  - GameController.playerShip.x + x,
            GameController.height / 2  - GameController.playerShip.y + y
        ];
    }

    mouseIsInside() {
        const [screenX, screenY] = this.getOnscreenCoords();

        if (dist(screenX, screenY, mouseX, mouseY) < Math.max(this.width, this.height) / 2) {
            return true;
        }

        // if ((mouseX >= screenX - this.width / 2 && mouseX <= screenX + this.width / 2) &&
        //     (mouseX >= screenX - this.width / 2 && mouseX <= screenX + this.width / 2)) {
        //     return true;
        // }

        // this._children.forEach(child => {
        //     if (child.mouseIsInside()) {
        //         return true;
        //     }
        // });

        return false;
    }
}

SpaceshipPart.MountPoint = class {
    constructor(radius, theta, part) {
        this.radius = radius;
        this.theta = theta;
        this.part = part;
    }

    setPart(part) {
        return new SpaceshipPart.MountPoint(this.radius, this.theta, part);
    }

    getX() {
        return this.radius * Math.cos(this.theta);
    }

    getY() {
        return this.radius * Math.sin(this.theta);
    }

    getGlobalCoords() {
        let [x, y, theta] = this.part.getGlobalCoords();
        
        theta = this.theta + this.part.theta;
        x = x + Math.cos(theta) * this.radius;
        y = y + Math.sin(theta) * this.radius;

        return [x, y, theta];
    }
}