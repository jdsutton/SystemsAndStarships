[[GameController.js]]

function setup() {
    const w = document.documentElement.clientWidth;
    const h = document.documentElement.clientHeight;
    
    createCanvas(w, h);
    GameController.setDimensions(w, h);

    angleMode(RADIANS);
}

function draw() {
    GameController.step();
}

function keyPressed() {
    GameController.keys[keyCode] = true;
}

function keyReleased() {
    GameController.keys[keyCode] = false;
}

function mouseClicked() {
    GameController.onClick();
}

window.addEventListener('resize', () => {
    const w = document.documentElement.clientWidth;
    const h = document.documentElement.clientHeight;

    resizeCanvas(w, h);
    
    GameController.setDimensions(w, h);
});